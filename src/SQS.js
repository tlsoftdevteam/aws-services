var AWS = require('aws-sdk');
const { Consumer } = require('sqs-consumer');
const https = require('https');
// AWS.config.loadFromPath('./.aws_credential.json');
let queues = {
  'sports4me': 'https://sqs.eu-central-1.amazonaws.com/762110983066/sports4me'
}
var sqs = new AWS.SQS({});

class SQSService {
  async createQueue(queueName) {
    return await new Promise((resolve, reject) => {
      var params = {
        QueueName: queueName,
        Attributes: {
          'DelaySeconds': '30',
          'MessageRetentionPeriod': '86400'
        }
      };

      sqs.createQueue(params, function(err, data) {
        if (err) {
          console.log("Error", err);
          reject(err);
        } else {
          console.log("Success createQueue:", data.QueueUrl);
          resolve(data.QueueUrl);
        }
      });
    })
  }

  async listQueues() {
    return await new Promise((resolve, reject) => {
      var params = {QueueNamePrefix:"sports4me"};
      sqs.listQueues(params, function(err, data) {
        if (err) {
          console.log("Error", err);
          reject(err);
        } else {
          // console.log("Success listQueues:", data.QueueUrls);
          resolve(data.QueueUrls);
        }
      });
    })
  }

  async getQueueUrl(queueName) {
    if (queues[queueName]) return queues[queueName];
    return await new Promise((resolve, reject) => {
      var self = this;
      var params = {
        QueueName: queueName
      };

      sqs.getQueueUrl(params, async function(err, data) {
        if (err) {
          if (err.code = 'AWS.SimpleQueueService.NonExistentQueue') {
            queues[queueName] = await self.createQueue(queueName);
            return resolve(queues[queueName]);
          }
          console.error("Error", err);
          reject(err);
        } else {
          queues[queueName] = data.QueueUrl;
          // console.log("Success getQueueUrl:", data.QueueUrl);
          resolve(queues[queueName]);
        }
      });
    })
  }

  async deleteQueue(queueName) {
    let queueURL = await this.getQueueUrl(queueName);
    return await new Promise((resolve, reject) => {
      var params = {
        QueueUrl: queueURL
      };

      sqs.deleteQueue(params, function(err, data) {
        if (err) {
          console.error("Error", err);
          reject(err);
        } else {
          // console.log("Success", data);
          resolve(data);
        }
      });
    })
  }

  async sendMessage(queueName, data) {
    if(!data.body) {
      console.error('sendMessage did not get any body!');
      return false;
    }
    let queueURL = await this.getQueueUrl(queueName);
    return await new Promise((resolve, reject) => {
      var params = {
          // Remove DelaySeconds parameter and value for FIFO queues
        DelaySeconds: 10,
        MessageAttributes:(typeof data.attr === "object") ? data.attr : {},
        /*{
          "Title": {
            DataType: "String",
            StringValue: "The Whistler"
          },
          "WeeksOn": {
            DataType: "Number",
            StringValue: "6"
          }
        },
        */
        MessageBody: (typeof data.body === "object") ? JSON.stringify(data.body) : data.body,
        // MessageDeduplicationId: "TheWhistler",  // Required for FIFO queues
        // MessageGroupId: "Group1",  // Required for FIFO queues
        QueueUrl: queueURL
      };

      sqs.sendMessage(params, function(err, data) {
        if (err) {
          console.log("Error", err);
          reject(err);
        } else {
          // console.log("Success", data.MessageId);
          resolve(true);
        }
      });
    })
  }

  async receiveMessage(queueName, deleteMsg=false) {
    let queueURL = await this.getQueueUrl(queueName);
    return await new Promise((resolve, reject) => {
      var params = {
        AttributeNames: [
          "SentTimestamp"
        ],
        MaxNumberOfMessages: 10,
        MessageAttributeNames: [
          "All"
        ],
        QueueUrl: queueURL,
        VisibilityTimeout: 20,
        WaitTimeSeconds: 0
      };

      sqs.receiveMessage(params, function(err, data) {
        if (err) {
          console.log("Error", err);
          reject(err);
        } else if (data.Messages) {
          for (let i = 0;i<data.Messages.length;i++) {
            console.log(data.Messages[i]);
            if(deleteMsg) {
              var deleteParams = {
                QueueUrl: queueURL,
                ReceiptHandle: data.Messages[i].ReceiptHandle
              };
              sqs.deleteMessage(deleteParams, function(err, data) {
                if (err) {
                  console.log("Error", err);
                  reject(err);
                } else {
                  console.log("Message Deleted", data);
                  resolve(true);
                }
              });
            }
          }
        }
      });
    })
  }
  async startPulling(queueName, cbFunction) {
    let queueURL = await this.getQueueUrl(queueName);
    console.log('start pulling queue:', queueURL);
    const consumer = Consumer.create({
      queueUrl: queueURL,
      messageAttributeNames: ["All"],
      handleMessage: (message) => {
        let body = null;
        let attr = {};
        try {
          attr = message.MessageAttributes || {};
          body = JSON.parse(message.Body);
        } catch(err) {
          console.error('JSON parse:', err);
          body = message.Body;
        }
        if(cbFunction && typeof cbFunction === 'function')
          cbFunction({body: body, attr: attr});
        else
          console.log('didnot get callBackFunction!');
        // do some work with `message`
      },
      sqs: new AWS.SQS({
        httpOptions: {
          agent: new https.Agent({
            keepAlive: true
          })
        }
      })
    });

    consumer.on('error', (err) => {
      console.error(err.message);
    });

    consumer.on('processing_error', (err) => {
      console.error(err.message);
    });

    consumer.start();
    process.on('SIGINT', () => {
      console.log("SIGINT Received, stopping consumer");
      consumer.stop();
      setTimeout(process.exit, 1000);
    });
  }

}

// createQueue('TST');
// listQueues();
// getQueueUrl('sports4me');

// deleteQueue('https://sqs.eu-central-1.amazonaws.com/762110983066/TST');

// sendMessage(queues['sports4me']);
// receiveMessage(queues['sports4me'], true);

module.exports = new SQSService();


